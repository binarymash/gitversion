﻿using System.Diagnostics;

namespace TestingGitVersion
{
    public class Program
    {
        public static void Main(string[] args)
        {
            //blah
            var location = System.Reflection.Assembly.GetExecutingAssembly().Location;
            var versionInfo = FileVersionInfo.GetVersionInfo(location);

            System.Console.WriteLine("FileVersion: {0}", versionInfo.FileVersion);
            System.Console.WriteLine("ProductVersion: {0}", versionInfo.ProductVersion);
            System.Console.ReadLine();
        }
    }
}
